/**
 * @file Phase10Player.cpp
 *
 * @brief State data for Phase 10 player
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Phase10/Card.h>
#include <Phase10/Deck.h>
#include <Phase10/Game.h>
#include <Phase10/Match.h>
#include <Phase10/Messages.h>
#include <Phase10/Phase.h>
#include <Phase10/Player.h>
#include <Phase10/User.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Player::Player(const User* pUser, sDeck pDrawPile, sDeck pDiscardPile) :
    mUser(pUser), mDrawPile(pDrawPile), mDiscardPile(pDiscardPile)
  {
    // Initialize variables
    mPhaseNum = PHASE_FIRST;
    mScore    = 0;
    mSkipped  = false;

    // Initialize hand
    mHand = std::make_shared<Deck>(MODE_SORT_VALUE);
    mHand->addListener(std::bind(&Player::handPhaseUpdate, this));

    // Reset phase
    mState = PLAYER_INIT;
    advanceRound(true);
  }

  /////////////////////////////////////////////////////////////////////////////
  // addListener
  /////////////////////////////////////////////////////////////////////////////
  void Player::addListener(const Listener& listener)
  {
    mListeners.push_back(listener);
  }

  /////////////////////////////////////////////////////////////////////////////
  // advanceRound
  /////////////////////////////////////////////////////////////////////////////
  void Player::advanceRound(bool reset)
  {
    if(mState == PLAYER_INIT)
    {
      // Advance phase if complete
      if(reset)
      {
        mPhaseNum = PHASE_FIRST;
        mScore    = 0;
      }
      else if(mPhase && mPhase->isComplete())
      {
        ++mPhaseNum;
      }

      // Return phase cards to draw pile
      if(mPhase)
      {
        mPhase->setLocked(false);
        mPhase->passAll(*mDrawPile);
      }

      // Initialize phase
      mPhaseComplete = false;
      mPhase = Phase::create(mPhaseNum);
      mPhase->addListener(std::bind(&Player::handPhaseUpdate, this));

      notify();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // canDrawFromDiscard
  /////////////////////////////////////////////////////////////////////////////
  bool Player::canDrawFromDiscard() const
  {
    bool result = false;
    if(mState == PLAYER_INIT || mState == PLAYER_IDLE)
    {
      Card topCard;
      if(mDiscardPile->peek(topCard))
      {
        result = !topCard.isSkip();
      }
    }

    return result;
  }

  /////////////////////////////////////////////////////////////////////////////
  // discard
  /////////////////////////////////////////////////////////////////////////////
  bool Player::discard(const Card& card)
  {
    bool success = false;
    if(mState == PLAYER_ACTIVE)
    {
      success = mHand->passTo(card, *mDiscardPile);
      if(success)
      {
        // If we added cards to phase but did not complete, return them
        if(!mPhase->isComplete())
        {
          mPhase->passAll(*mHand);
        }

        // If another player completes their phase, Game will unlock phase
        setPhaseLocked(true);

        // Catch when out of cards
        if(mHand->empty())
        {
          mState = PLAYER_OUT;
        }
        else if(card.isSkip())
        {
          mState = PLAYER_SKIP;
        }
        else
        {
          mState = PLAYER_INACTIVE;
        }

        // Notify listeners that this player is done
        mHand->setLocked(true);
        notify();
      }
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // draw
  /////////////////////////////////////////////////////////////////////////////
  bool Player::draw()
  {
    return drawFrom(*mDrawPile);
  }

  /////////////////////////////////////////////////////////////////////////////
  // drawFromDiscard
  /////////////////////////////////////////////////////////////////////////////
  bool Player::drawFromDiscard()
  {
    return(canDrawFromDiscard() && drawFrom(*mDiscardPile));
  }

  /////////////////////////////////////////////////////////////////////////////
  // getPhaseString
  /////////////////////////////////////////////////////////////////////////////
  std::string Player::getPhaseString() const
  {
    static const std::string PHASE("Phase ");
    return(PHASE + std::to_string(mPhaseNum));
  }

  /////////////////////////////////////////////////////////////////////////////
  // getPlayerState
  /////////////////////////////////////////////////////////////////////////////
  void Player::getPlayerState(Messages::PlayerStateMsg& msg) const
  {
    msg.playerID = id();
    msg.score = mScore;
    msg.skipped = mSkipped;
    msg.state = mState;
    msg.phaseNum = mPhaseNum;
    mHand->getCards(msg.hand);
    if(mPhase)
    {
      mPhase->getPhase(msg.phase);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // leave
  /////////////////////////////////////////////////////////////////////////////
  void Player::leave()
  {
    mState = PLAYER_QUIT;

    mHand->setLocked(false);
    mHand->passAll(*mDrawPile);
    mHand->setLocked(true);

    if(mPhase)
    {
      mPhase->setLocked(false);
      mPhase->passAll(*mDrawPile);
      mPhase->setLocked(true);
    }

    notify();
  }

  /////////////////////////////////////////////////////////////////////////////
  // setMode
  /////////////////////////////////////////////////////////////////////////////
  void Player::setMode(DeckMode mode)
  {
    mHand->setMode(mode);
  }

  /////////////////////////////////////////////////////////////////////////////
  // setPlayerState
  /////////////////////////////////////////////////////////////////////////////
  void Player::setPlayerState(const Messages::PlayerStateMsg& msg)
  {
    if(msg.playerID == id())
    {
      mScore   = msg.score;
      mSkipped = msg.skipped;
      mState   = msg.state;
      mHand->setCards(msg.hand);

      if(mPhase == nullptr || msg.phaseNum != mPhaseNum)
      {
        mPhase = Phase::create(msg.phaseNum);
        mPhase->addListener(std::bind(&Player::handPhaseUpdate, this));
      }

      mPhaseNum = msg.phaseNum;
      mPhase->setPhase(msg.phase);
      mPhaseComplete = mPhase->isComplete();

      notify();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // skipPlayer
  /////////////////////////////////////////////////////////////////////////////
  bool Player::skipPlayer(Player& player)
  {
    bool success = false;
    if(mState == PLAYER_SKIP && player.setSkipped())
    {
      mState  = PLAYER_INACTIVE;
      success = true;
      notify();
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // startRound
  /////////////////////////////////////////////////////////////////////////////
  bool Player::startRound()
  {
    bool success = false;
    if(mState == PLAYER_INIT)
    {
      mHand->setLocked(true);
      mPhase->setLocked(true);
      mState  = PLAYER_INACTIVE;
      success = true;
      notify();
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // startTurn
  /////////////////////////////////////////////////////////////////////////////
  bool Player::startTurn()
  {
    bool success = false;
    if(mState == PLAYER_INACTIVE)
    {
      if(mSkipped)
      {
        mSkipped = false;
      }
      else
      {
        setPhaseLocked(false);
        mHand->setLocked(false);
        mState = PLAYER_IDLE;
      }

      success = true;
      notify();
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // drawFrom
  /////////////////////////////////////////////////////////////////////////////
  bool Player::drawFrom(Deck& deck)
  {
    bool success = false;
    if(mState == PLAYER_INIT || mState == PLAYER_IDLE)
    {
      success = deck.passTo(*mHand);
      if(success && mState == PLAYER_IDLE)
      {
        mPhase->setLocked(false);
        mState = PLAYER_ACTIVE;
        notify();
      }
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // endRound
  /////////////////////////////////////////////////////////////////////////////
  bool Player::endRound()
  {
    bool success = false;
    if(mState == PLAYER_INACTIVE || mState == PLAYER_OUT)
    {
      // Add score
      success = true;
      mSkipped = false;
      mScore += mHand->getPoints();

      // Send everything back to draw pile
      mHand->setLocked(false);
      mHand->passAll(*mDrawPile);
      mState = PLAYER_INIT;
      notify();
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // handPhaseUpdate
  /////////////////////////////////////////////////////////////////////////////
  void Player::handPhaseUpdate()
  {
    if(mPhase->isComplete())
    {
      mPhaseComplete = true;
      if(mState == PLAYER_ACTIVE)
      {
        if(mHand->empty())
        {
          mHand->setLocked(true);
          mState = PLAYER_OUT;
        }

        // Only notify state update for the active player
        notify();
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // notify
  /////////////////////////////////////////////////////////////////////////////
  void Player::notify()
  {
    for(auto& listener : mListeners)
    {
      listener();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // setPhaseLocked
  /////////////////////////////////////////////////////////////////////////////
  void Player::setPhaseLocked(bool locked)
  {
    mPhase->setLocked(locked);
  }

  /////////////////////////////////////////////////////////////////////////////
  // setSkipped
  /////////////////////////////////////////////////////////////////////////////
  bool Player::setSkipped()
  {
    bool success = false;
    if(!mSkipped)
    {
      mSkipped = true;
      success  = true;
      notify();
    }

    return success;
  }

} // namespace Phase10
