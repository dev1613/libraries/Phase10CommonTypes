/**
 * @file Game.cpp
 *
 * @brief Represents a unique game for the Phase 10 system
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <set>

#include <Phase10/Deck.h>
#include <Phase10/Game.h>
#include <Phase10/Messages.h>
#include <Phase10/Phase.h>
#include <Phase10/Player.h>
#include <Phase10/User.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // getFullDeckParams
  /////////////////////////////////////////////////////////////////////////////
  static DeckParams getFullDeckParams()
  {
    // Constants
    static constexpr size_t NUM_STANDARD_EACH = 2;
    static constexpr size_t NUM_SKIP = 4;
    static constexpr size_t NUM_WILD = 8;

    // Add standard cards
    DeckParams params;
    for(CardColor color : { COLOR_RED, COLOR_BLUE, COLOR_GREEN, COLOR_YELLOW })
    {
      for(uint32_t value = VALUE_STD_MIN; value <= VALUE_STD_MAX; ++value)
      {
        // 2 of each standard card
        params.emplace_back(Card(value, color), NUM_STANDARD_EACH);
      }
    }

    // Add special cards
    params.emplace_back(Card(VALUE_WILD), NUM_WILD);
    params.emplace_back(Card(VALUE_SKIP), NUM_SKIP);

    return std::move(params);
  }

  // Constants
  static const DeckParams FULL_DECK(getFullDeckParams());

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Game::Game(System::sLogger pLogger, uint32_t id, const std::string& name, bool master) :
      mLogger(pLogger), mGameInfo(id, name), mMaster(master)
  {
    mDrawPile    = std::make_shared<Deck>(FULL_DECK, MODE_LIFO);
    mDiscardPile = std::make_shared<Deck>(MODE_FIFO);
    mMsgUpdate   = false;

    mLogger->debug("Created game %s with ID %u", name.c_str(), id);
  }

  /////////////////////////////////////////////////////////////////////////////
  // addPlayer
  /////////////////////////////////////////////////////////////////////////////
  sPlayer Game::addPlayer(const User* pUser)
  {
    sPlayer pPlayer;
    if(!mGameInfo.canAddPlayers())
    {
      mLogger->error("Game::%s - %s [%u] is full",
                      __func__, name().c_str(), id());
    }
    else
    {
      pPlayer = std::make_shared<Player>(pUser, mDrawPile, mDiscardPile);
      pPlayer->addListener(std::bind(&Game::playerUpdateCallback, this, pPlayer, true));

      mPlayers.push_back(pPlayer);
      mGameInfo.mNumPlayers = mPlayers.size();

      mLogger->info("Game::%s - Added %s [%u] to game %s [%u]",
                    __func__, pUser->name().c_str(), pUser->id(),
                    name().c_str(), id());
    }

    return pPlayer;
  }

  /////////////////////////////////////////////////////////////////////////////
  // addPlayerListener
  /////////////////////////////////////////////////////////////////////////////
  void Game::addPlayerListener(const PlayerListener& listener)
  {
    mPlayerListeners.push_back(listener);
  }

  /////////////////////////////////////////////////////////////////////////////
  // addDrawPileListener
  /////////////////////////////////////////////////////////////////////////////
  void Game::addDrawPileListener(const DeckListener& listener)
  {
    mDrawPile->addListener(listener);
  }

  /////////////////////////////////////////////////////////////////////////////
  // addDiscardPileListener
  /////////////////////////////////////////////////////////////////////////////
  void Game::addDiscardPileListener(const DeckListener& listener)
  {
    mDiscardPile->addListener(listener);
  }

  /////////////////////////////////////////////////////////////////////////////
  // getInfo
  /////////////////////////////////////////////////////////////////////////////
  void Game::getInfo(Messages::GameInfoMsg& msg) const
  {
    msg.id = mGameInfo.mID;
    msg.name = mGameInfo.mName;
    msg.numPlayers = mGameInfo.mNumPlayers;
    msg.state = mGameInfo.mState;
  }

  /////////////////////////////////////////////////////////////////////////////
  // getMembers
  /////////////////////////////////////////////////////////////////////////////
  void Game::getMembers(Messages::GameMembersMsg& msg) const
  {
    msg.gameID = mGameInfo.mID;
    msg.players.clear();
    for(auto& pPlayer : mPlayers)
    {
      auto& playerMsg = msg.players.emplace_back();
      playerMsg.id = pPlayer->id();
      playerMsg.name = pPlayer->name();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // getPlayer
  /////////////////////////////////////////////////////////////////////////////
  sPlayer Game::getPlayer(uint32_t playerID) const
  {
    sPlayer pPlayer;
    auto it = findPlayer(playerID);
    if(it != mPlayers.end())
    {
      pPlayer = *it;
    }

    return pPlayer;
  }

  /////////////////////////////////////////////////////////////////////////////
  // getState
  /////////////////////////////////////////////////////////////////////////////
  void Game::getState(Messages::GameStateMsg& msg) const
  {
    msg.gameID          = mGameInfo.mID;
    msg.startPlayerID   = (mStartPlayer ? mStartPlayer->id() : GameFactory::INVALID_ID);
    msg.currentPlayerID = (mCurrentPlayer ? mCurrentPlayer->id() : GameFactory::INVALID_ID);
    msg.winnerID        = (mWinner ? mWinner->id() : GameFactory::INVALID_ID);
    msg.state           = mGameInfo.mState;
    mDrawPile->getCards(msg.drawPile);
    mDiscardPile->getCards(msg.discardPile);

    msg.players.clear();
    for(auto& pPlayer : mPlayers)
    {
      pPlayer->getPlayerState(msg.players.emplace_back());
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // removePlayer
  /////////////////////////////////////////////////////////////////////////////
  void Game::removePlayer(uint32_t userID)
  {
    auto it = findPlayer(userID);
    if(it != mPlayers.end())
    {
      auto pPlayer = *it;
      pPlayer->leave();
      mPlayers.erase(it);
      mGameInfo.mNumPlayers = mPlayers.size();
      mLogger->debug("Game::%s - Removed %s [%u] from %s [%u]", __func__,
                      pPlayer->name().c_str(), pPlayer->id(),
                      name().c_str(), id());
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // setMembers
  /////////////////////////////////////////////////////////////////////////////
  void Game::setMembers(const Messages::GameMembersMsg& msg, UserFactory& userFactory)
  {
    if(id() == msg.gameID)
    {
      // Create users if they don't exist
      std::set<uint32_t> playerIDs;
      std::list<sUser> playersToAdd;
      for(auto& player : msg.players)
      {
        playerIDs.insert(player.id);
        auto pUser = userFactory.findOrCreate(player.id, player.name);
        if(findPlayer(player.id) == mPlayers.end())
        {
          playersToAdd.push_back(pUser);
        }
      }

      // Get players to delete
      std::list<sUser> playersToDelete;
      for(auto& pPlayer : mPlayers)
      {
        if(playerIDs.find(pPlayer->id()) == playerIDs.end())
        {
          auto pUser = userFactory.find(pPlayer->id());
          playersToDelete.push_back(pUser);
        }
      }

      // Delete players
      bool update = false;
      for(auto& pPlayer : playersToDelete)
      {
        pPlayer->leave();
      }

      // Add players
      for(auto& pPlayer : playersToAdd)
      {
        pPlayer->join(this);
      }
    }
    else
    {
      mLogger->warn("Game::%s - Got member update for another game", __func__);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // setState
  /////////////////////////////////////////////////////////////////////////////
  void Game::setState(const Messages::GameStateMsg& msg)
  {
    bool playerAdvanced = false;
    setState(msg, playerAdvanced);
  }

  /////////////////////////////////////////////////////////////////////////////
  // setState
  /////////////////////////////////////////////////////////////////////////////
  void Game::setState(const Messages::GameStateMsg& msg, bool& playerAdvanced)
  {
    playerAdvanced = false;
    if(id() == msg.gameID)
    {
      mMsgUpdate = true;

      auto it = findPlayer(msg.startPlayerID);
      if(it != mPlayers.end())
      {
        mStartPlayer = *it;
      }

      it = findPlayer(msg.currentPlayerID);
      if(it != mPlayers.end())
      {
        mCurrentPlayer = *it;
      }

      it = findPlayer(msg.winnerID);
      if(it != mPlayers.end())
      {
        mWinner = *it;
      }
      else
      {
        mWinner = nullptr;
      }

      mGameInfo.mState = msg.state;
      mDrawPile->setCards(msg.drawPile);
      mDiscardPile->setCards(msg.discardPile);

      for(auto& playerMsg : msg.players)
      {
        auto it = findPlayer(playerMsg.playerID);
        if(it != mPlayers.end())
        {
          auto& pPlayer = *it;
          pPlayer->setPlayerState(playerMsg);
        }
        else
        {
          mLogger->error("Game::%s - Player %u is not in game %s [%u]",
            __func__, playerMsg.playerID, name().c_str(), id());
        }
      }

      // Advance current player
      mMsgUpdate = false;
      playerUpdate(mCurrentPlayer, false, playerAdvanced);
    }
    else
    {
      mLogger->warn("Game::%s - Got state update for another game", __func__);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // startRound
  /////////////////////////////////////////////////////////////////////////////
  bool Game::startRound()
  {
    // Constants
    static constexpr size_t CARDS_PER_PLAYER = 10;

    // Cannot start if there aren't enough players
    if(!ready())
    {
      mLogger->error("Game::%s - Only %u players present in %s [%u]",
                      __func__, numPlayers(), name().c_str(), id());
      return false;
    }

    // Check state
    bool resetPlayers = false;
    switch(mGameInfo.mState)
    {
      case GAME_INIT:
      {
        mStartPlayer = mPlayers.front();
        break;
      }

      case GAME_OVER:
      {
        mWinner = nullptr;
        resetPlayers = true;
        break;
      }

      case GAME_ACTIVE:
      {
        mLogger->error("Game::%s - %s [%u] is already in active state",
                        __func__, name().c_str(), id());
        return false;
      }

      case GAME_QUIT:
      {
        mLogger->error("Game::%s - %s [%u] has lost too many players",
                        __func__, name().c_str(), id());
        return false;
      }

      default:
      {
        break;
      }
    }

    // Return all player cards to draw pile
    for(auto& pPlayer : mPlayers)
    {
      pPlayer->advanceRound(resetPlayers);
    }

    // Return all discards to the draw pile
    mDiscardPile->passAll(*mDrawPile);

    // Shuffle draw pile
    mDrawPile->shuffle();

    // Deal cards
    for(size_t i = 0; i < CARDS_PER_PLAYER; ++i)
    {
      for(auto& pPlayer : mPlayers)
      {
        pPlayer->draw();
      }
    }

    // Start round
    for(auto& pPlayer : mPlayers)
    {
      pPlayer->startRound();
    }

    // Deal top card onto discard pile
    mDrawPile->passTo(*mDiscardPile);

    // Activate starting player
    mGameInfo.mState = GAME_ACTIVE;
    mCurrentPlayer   = mStartPlayer;

    auto it = findPlayer(mStartPlayer);
    if(it == mPlayers.end())
    {
      mLogger->error("Game::%s - %s [%u] Start player not found",
                      __func__, name().c_str(), id());
      return false;
    }
    else
    {
      mStartPlayer = *nextPlayer(it);
    }

    mCurrentPlayer->startTurn();
    mLogger->debug("Game::%s - Started %s [%u] round with first player %s [%u]",
                    __func__, name().c_str(), id(), mCurrentPlayer->name().c_str(),
                    mCurrentPlayer->id());

    return true;
  }

  /////////////////////////////////////////////////////////////////////////////
  // findPlayer
  /////////////////////////////////////////////////////////////////////////////
  Game::PlayerList::iterator Game::findPlayer(sPlayer pPlayer)
  {
    return std::find(mPlayers.begin(), mPlayers.end(), pPlayer);
  }

  /////////////////////////////////////////////////////////////////////////////
  // comparePlayer
  /////////////////////////////////////////////////////////////////////////////
  static bool comparePlayer(uint32_t id, const sPlayer& pPlayer)
  {
    return(pPlayer->id() == id);
  }

  /////////////////////////////////////////////////////////////////////////////
  // findPlayer
  /////////////////////////////////////////////////////////////////////////////
  Game::PlayerList::iterator Game::findPlayer(uint32_t id)
  {
    using namespace std::placeholders;
    return std::find_if(mPlayers.begin(), mPlayers.end(), std::bind(&comparePlayer, id, _1));
  }

  /////////////////////////////////////////////////////////////////////////////
  // findPlayer
  /////////////////////////////////////////////////////////////////////////////
  Game::PlayerList::const_iterator Game::findPlayer(uint32_t id) const
  {
    using namespace std::placeholders;
    return std::find_if(mPlayers.begin(), mPlayers.end(), std::bind(&comparePlayer, id, _1));
  }

  /////////////////////////////////////////////////////////////////////////////
  // playerUpdateCallback
  /////////////////////////////////////////////////////////////////////////////
  void Game::playerUpdateCallback(sPlayer pPlayer, bool notify)
  {
    bool playerAdvanced = false;
    playerUpdate(pPlayer, notify, playerAdvanced);
  }

  /////////////////////////////////////////////////////////////////////////////
  // playerUpdate
  /////////////////////////////////////////////////////////////////////////////
  void Game::playerUpdate(sPlayer pPlayer, bool notify, bool& playerAdvanced)
  {
    // Forward to player listeners
    playerAdvanced = false;
    if(notify)
    {
      for(auto& listener : mPlayerListeners)
      {
        listener(pPlayer);
      }
    }

    // Ignore updates unless game is active
    if(mGameInfo.mState != GAME_ACTIVE || !mMaster || mMsgUpdate)
    {
      return;
    }

    // Check current player state
    auto it = findPlayer(pPlayer);
    PlayerState playerState = pPlayer->getState();

    if(it == mPlayers.end())
    {
      mLogger->error("Game::%s - Invalid player in %s [%u]",
                      __func__, name().c_str(), id());
      return;
    }

    mLogger->debug("Game::%s - %s [%u] in state %u",
      __func__, pPlayer->name().c_str(), pPlayer->id(), playerState);

    switch(playerState)
    {
      // Player quit
      case PLAYER_QUIT:
      {
        if(pPlayer == mStartPlayer)
        {
          mStartPlayer = *nextPlayer(it);
        }

        if(numPlayers() <= 2)
        {
          mLogger->debug("Game::%s - Not enough players to continue game", __func__);
          mGameInfo.mState = GAME_QUIT;
        }

        // Intentional fall through to advance current player
      }

      // Player discarded
      case PLAYER_INACTIVE:
      {
        if(pPlayer == mCurrentPlayer)
        {
          mCurrentPlayer = *nextPlayer(it);
          mCurrentPlayer->startTurn();
          playerAdvanced = true;
          mLogger->debug("Game::%s - Advanced %s [%u] player to %s [%u]",
                          __func__, name().c_str(), id(),
                          mCurrentPlayer->name().c_str(), mCurrentPlayer->id());
        }
        break;
      }

      // Player active but may have completed phase
      case PLAYER_ACTIVE:
      {
        if(pPlayer == mCurrentPlayer)
        {
          for(auto& pOpponent : mPlayers)
          {
            if(pOpponent != pPlayer)
            {
              bool open = (pPlayer->isPhaseComplete() && pOpponent->isPhaseComplete());
              pOpponent->setPhaseLocked(!open);
            }
          }

          if(pPlayer->isPhaseComplete())
          {
            playerAdvanced = true;
          }
        }
        break;
      }

      // Player discarded skip card
      case PLAYER_SKIP:
      {
        if(pPlayer == mCurrentPlayer && numPlayers() == 2)
        {
          // Automatically skip other player
          mCurrentPlayer->skipPlayer(**nextPlayer(it));
          playerAdvanced = true;
          mLogger->debug("Game::%s - Skipped %s [%u] opponent in %s [%u]",
            __func__, pPlayer->name().c_str(), pPlayer->id(),
            name().c_str(), id());
        }
        break;
      }

      // Player is out of cards
      case PLAYER_OUT:
      {
        playerAdvanced = true;
        mGameInfo.mState = GAME_ROUND_END;
        mLogger->debug("Game::%s - %s [%u] Round over, %s [%u] out of cards",
                        __func__, name().c_str(), id(),
                        mCurrentPlayer->name().c_str(), mCurrentPlayer->id());

        // Check for winner
        for(auto& otherPlayer : mPlayers)
        {
          otherPlayer->endRound();

          PhaseNumber phaseNum = otherPlayer->getPhaseNum();
          auto pPhase = otherPlayer->getPhase();
          if(pPhase->isComplete() && phaseNum == PHASE_LAST)
          {
            mGameInfo.mState = GAME_OVER;
            if(mWinner == nullptr || otherPlayer->getScore() < mWinner->getScore())
            {
              mWinner = otherPlayer;
            }
          }
        }

        // Log winner
        if(mWinner)
        {
          mLogger->info("Game::%s - %s [%u] Game over, won by %s [%u]",
                        __func__, name().c_str(), id(),
                        mWinner->name().c_str(), mWinner->id());
        }
        break;
      }

      // Don't care about other state transitions
      default:
      {
        break;
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // nextPlayer
  /////////////////////////////////////////////////////////////////////////////
  Game::PlayerList::iterator Game::nextPlayer(PlayerList::iterator player)
  {
    auto next = std::next(player);
    if(next == mPlayers.end())
    {
      next = mPlayers.begin();
    }

    return next;
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  GameFactory::GameFactory(System::sLogger pLogger, bool master) :
    mMaster(master), mLogger(pLogger)
  {
    mIDs.take(INVALID_ID);
  }

  /////////////////////////////////////////////////////////////////////////////
  // create
  /////////////////////////////////////////////////////////////////////////////
  sGame GameFactory::create(uint32_t id, const std::string& name)
  {
    sGame pGame;
    if(mIDs.take(id))
    {
      pGame = Base::create(mLogger, id, name, mMaster);
    }

    return pGame;
  }

  /////////////////////////////////////////////////////////////////////////////
  // create
  /////////////////////////////////////////////////////////////////////////////
  sGame GameFactory::create(const std::string& name)
  {
    sGame pGame;
    uint32_t id = 0;
    if(mIDs.takeNext(id))
    {
      pGame = Base::create(mLogger, id, name, mMaster);
    }

    return pGame;
  }

  /////////////////////////////////////////////////////////////////////////////
  // find
  /////////////////////////////////////////////////////////////////////////////
  sGame GameFactory::find(uint32_t id)
  {
    return Base::find([id](const Game& game) { return(game.id() == id); });
  }

  /////////////////////////////////////////////////////////////////////////////
  // findOrCreate
  /////////////////////////////////////////////////////////////////////////////
  sGame GameFactory::findOrCreate(uint32_t id, const std::string& name)
  {
    auto pGame = find(id);
    if(pGame == nullptr)
    {
      pGame = create(id, name);
    }

    return pGame;
  }

} // namespace Phase10
