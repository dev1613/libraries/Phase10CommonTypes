/**
 * @file Phase10Run.h
 *
 * @brief Classes to represent run of cards
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <Phase10/Match.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       Run
  // Description: Sequential run of cards
  //-------------------------------------------------------------------------//
  class Run : public Match
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param minSize - minimum size
    ///////////////////////////////////////////////////////////////////////////
    Run(size_t minSize);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~Run() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Determines if the match is complete
    /// @return - true if complete, else false
    ///////////////////////////////////////////////////////////////////////////
    virtual bool isComplete() const;

  protected:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Check if card can be added
    /// @param card - card to add
    /// @param front - add to front?
    /// @return - true if passed, else false
    ///////////////////////////////////////////////////////////////////////////
    virtual bool canAdd(const Card& card, bool front) const;

  private:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the first value in the run
    /// @return - card value, or wild if none yet
    ///////////////////////////////////////////////////////////////////////////
    CardValue getFirstValue() const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the last value in the run
    /// @return - card value, or wild if none yet
    ///////////////////////////////////////////////////////////////////////////
    CardValue getLastValue() const;
  };

} // namespace Phase10
