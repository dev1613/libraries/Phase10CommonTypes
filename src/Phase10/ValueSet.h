/**
 * @file Phase10ValueSet.h
 *
 * @brief Classes to represent set of same value
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <Phase10/Match.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       ValueSet
  // Description: Set of cards with matching value
  //-------------------------------------------------------------------------//
  class ValueSet : public Match
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param minSize - minimum size
    ///////////////////////////////////////////////////////////////////////////
    ValueSet(size_t minSize);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~ValueSet() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Determines if the match is complete
    /// @return - true if complete, else false
    ///////////////////////////////////////////////////////////////////////////
    virtual bool isComplete() const;

  protected:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Check if card can be added
    /// @param card - card to add
    /// @param front - add to front?
    /// @return - true if passed, else false
    ///////////////////////////////////////////////////////////////////////////
    virtual bool canAdd(const Card& card, bool front) const;

  private:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the value of the set
    /// @return - card value, or wild if none yet
    ///////////////////////////////////////////////////////////////////////////
    CardValue getValue() const;
  };

} // namespace Phase10
