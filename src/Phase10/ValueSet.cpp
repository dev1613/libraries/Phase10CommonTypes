/**
 * @file Phase10ValueSet.cpp
 *
 * @brief Classes to represent match (i.e. laid down) cards
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Phase10/Card.h>
#include <Phase10/Deck.h>
#include <Phase10/ValueSet.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  ValueSet::ValueSet(size_t minSize) : Match(minSize, "SET OF " + std::to_string(minSize))
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // isComplete
  /////////////////////////////////////////////////////////////////////////////
  bool ValueSet::isComplete() const
  {
    // Must have at least 1 non-wild card to be done
    return(Match::isComplete() && getValue() != VALUE_WILD);
  }

  /////////////////////////////////////////////////////////////////////////////
  // canAdd
  /////////////////////////////////////////////////////////////////////////////
  bool ValueSet::canAdd(const Card& card, bool front) const
  {
    // Direction doesn't matter for sets
    // 1 - Can add any wild card
    // 2 - Can add any standard card that matches the set value
    CardValue setValue = getValue();
    return(card.isWild() ||
           (card.isStandard() &&
              (setValue == card.getValue() || setValue == VALUE_WILD)));
  }

  /////////////////////////////////////////////////////////////////////////////
  // getValue
  /////////////////////////////////////////////////////////////////////////////
  CardValue ValueSet::getValue() const
  {
    // Assumption: All standard cards should match
    auto& deck = *Match::getDeck();
    for(auto& card : deck)
    {
      if(card.isStandard())
      {
        return card.getValue();
      }
    }

    // No non-wild cards
    return VALUE_WILD;
  }

} // namespace Phase10
