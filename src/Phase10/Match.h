/**
 * @file Phase10Match.h
 *
 * @brief Classes to represent match (i.e. laid down) cards
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <string>

#include <Phase10/CommonTypes.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       Match
  // Description: Base class for matches
  //-------------------------------------------------------------------------//
  class Match
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param minSize - minimum size
    /// @param name - name of match
    ///////////////////////////////////////////////////////////////////////////
    Match(size_t minSize, const std::string& name);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~Match() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Adds a card to the match
    /// @param card - card to add
    /// @param front - add to front?
    /// @return - true if added, else false
    ///////////////////////////////////////////////////////////////////////////
    bool add(const Card& card, bool front = false);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the deck
    /// @return - deck
    ///////////////////////////////////////////////////////////////////////////
    scDeck getDeck() const { return mDeck; }
    sDeck getDeck() { return mDeck; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Determines if the match is complete
    /// @return - true if complete, else false
    ///////////////////////////////////////////////////////////////////////////
    virtual bool isComplete() const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Determines if the deck is locked for editing
    /// @return - true if locked, else false
    ///////////////////////////////////////////////////////////////////////////
    bool isLocked() const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the minimum size
    /// @return - minimum size
    ///////////////////////////////////////////////////////////////////////////
    size_t minSize() const { return mMinSize; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the name of this match
    /// @return - match name
    ///////////////////////////////////////////////////////////////////////////
    const std::string& name() const { return mName; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Passes all the cards from the match to the given deck
    /// @param deck[i/o] - deck to pass to
    /// @return - true if passed, else false
    ///////////////////////////////////////////////////////////////////////////
    bool passAll(Deck& deck);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Adds a card to the match from another deck
    /// @param card - card to add
    /// @param deck[i/o] - deck to pass from
    /// @param front - add to front?
    /// @return - true if passed, else false
    ///////////////////////////////////////////////////////////////////////////
    bool passFrom(const Card& card, Deck& deck, bool front = false);

  protected:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Check if card can be added
    /// @param card - card to add
    /// @param front - add to front?
    /// @return - true if passed, else false
    ///////////////////////////////////////////////////////////////////////////
    virtual bool canAdd(const Card& card, bool front) const = 0;

  private:
    // Allow Phase to call setLocked
    friend class Phase;

    // Private class variables
    const size_t      mMinSize; ///< minimum size
    const std::string mName;    ///< match type name

    sDeck mDeck; ///< deck representing match cards

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sets whether the deck is locked for editing
    /// @param locked - true if locked, else false
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void setLocked(bool locked);
  };

} // namespace Phase10
