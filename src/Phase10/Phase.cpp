/**
 * @file Phase10Phase.cpp
 *
 * @brief Classes to describe phase
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Phase10/ColorSet.h>
#include <Phase10/Deck.h>
#include <Phase10/Match.h>
#include <Phase10/Messages.h>
#include <Phase10/Phase.h>
#include <Phase10/Run.h>
#include <Phase10/ValueSet.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Phase::Phase(MatchList&& matches) : mMatches(std::move(matches)) {}

  /////////////////////////////////////////////////////////////////////////////
  // addListener
  /////////////////////////////////////////////////////////////////////////////
  void Phase::addListener(const DeckListener& listener)
  {
    for(auto& pMatch : mMatches)
    {
      pMatch->getDeck()->addListener(listener);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // create (static)
  /////////////////////////////////////////////////////////////////////////////
  sPhase Phase::create(PhaseNumber phaseNum)
  {
    // Get matches based on phase
    MatchList matches;
    switch(phaseNum)
    {
      // Phase 1: 2 sets of 3
      case 1:
      {
        matches.push_back(std::make_shared<ValueSet>(3));
        matches.push_back(std::make_shared<ValueSet>(3));
        break;
      }

      // Phase 2: 1 set of 3, 1 run of 4
      case 2:
      {
        matches.push_back(std::make_shared<ValueSet>(3));
        matches.push_back(std::make_shared<Run>(4));
        break;
      }

      // Phase 3: 1 set of 4, 1 run of 4
      case 3:
      {
        matches.push_back(std::make_shared<ValueSet>(4));
        matches.push_back(std::make_shared<Run>(4));
        break;
      }

      // Phase 4: 1 run of 7
      case 4:
      {
        matches.push_back(std::make_shared<Run>(7));
        break;
      }

      // Phase 5: 1 run of 8
      case 5:
      {
        matches.push_back(std::make_shared<Run>(8));
        break;
      }

      // Phase 6: 1 run of 9
      case 6:
      {
        matches.push_back(std::make_shared<Run>(9));
        break;
      }

      // Phase 7: 2 sets of 4
      case 7:
      {
        matches.push_back(std::make_shared<ValueSet>(4));
        matches.push_back(std::make_shared<ValueSet>(4));
        break;
      }

      // Phase 8: 7 cards of 1 color
      case 8:
      {
        matches.push_back(std::make_shared<ColorSet>(7));
        break;
      }

      // Phase 9: 1 set of 5, 1 set of 2
      case 9:
      {
        matches.push_back(std::make_shared<ValueSet>(5));
        matches.push_back(std::make_shared<ValueSet>(2));
        break;
      }

      // Phase 10: 1 set of 5, 1 set of 3
      case 10:
      {
        matches.push_back(std::make_shared<ValueSet>(5));
        matches.push_back(std::make_shared<ValueSet>(3));
        break;
      }

      // Default: no phase
      default:
      {
        return nullptr;
      }
    }

    // Create phase
    std::shared_ptr<Phase> pPhase;
    pPhase.reset(new Phase(std::move(matches)));
    return pPhase;
  }

  /////////////////////////////////////////////////////////////////////////////
  // getPhase
  /////////////////////////////////////////////////////////////////////////////
  void Phase::getPhase(Messages::PhaseMsg& msg) const
  {
    msg.matches.clear();
    for(auto& pMatch : mMatches)
    {
      pMatch->getDeck()->getCards(msg.matches.emplace_back());
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // isComplete
  /////////////////////////////////////////////////////////////////////////////
  bool Phase::isComplete() const
  {
    for(auto& pMatch : mMatches)
    {
      if(!pMatch->isComplete())
      {
        return false;
      }
    }

    return true;
  }

  /////////////////////////////////////////////////////////////////////////////
  // isLocked
  /////////////////////////////////////////////////////////////////////////////
  bool Phase::isLocked() const
  {
    for(auto& pMatch : mMatches)
    {
      if(pMatch->isLocked())
      {
        return true;
      }
    }

    return false;
  }

  /////////////////////////////////////////////////////////////////////////////
  // passAll
  /////////////////////////////////////////////////////////////////////////////
  bool Phase::passAll(Deck& deck)
  {
    bool success = true;
    for(auto& pMatch : mMatches)
    {
      success = pMatch->passAll(deck) && success;
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // setPhase
  /////////////////////////////////////////////////////////////////////////////
  void Phase::setPhase(const Messages::PhaseMsg& msg)
  {
    if(mMatches.size() == msg.matches.size())
    {
      auto itMsg = msg.matches.begin();
      auto itMatch = mMatches.begin();
      for( ; itMsg != msg.matches.end() && itMatch != mMatches.end(); ++itMsg, ++itMatch)
      {
        auto& pMatch = *itMatch;
        pMatch->getDeck()->setCards(*itMsg);
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // setLocked
  /////////////////////////////////////////////////////////////////////////////
  void Phase::setLocked(bool locked)
  {
    for(auto& pMatch : mMatches)
    {
      pMatch->setLocked(locked);
    }
  }

} // namespace Phase10
