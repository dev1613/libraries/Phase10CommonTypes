/**
 * @file Messages.h
 *
 * @brief Declarations for Phase 10 messages
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <list>
#include <string>

#include <Phase10/Card.h>
#include <Phase10/CommonTypes.h>
#include <System/Serializable.h>

namespace Phase10
{
  namespace Messages
  {
    //-----------------------------------------------------------------------//
    // Class:       RequestUserMsg
    // Description: Message for requesting a user ID
    //-----------------------------------------------------------------------//
    struct RequestUserMsg : public System::Serializable
    {
      /////////////////////////////////////////////////////////////////////////
      /// @brief Constructor
      /////////////////////////////////////////////////////////////////////////
      RequestUserMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Destructor
      /////////////////////////////////////////////////////////////////////////
      virtual ~RequestUserMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Decodes the member data from the buffer
      /// @param buffer - buffer to append to
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      virtual bool decode(System::SerialBuffer& buffer);

      /////////////////////////////////////////////////////////////////////////
      /// @brief Encodes the member data into the buffer
      /// @param buffer - buffer to append to
      /// @return - None
      /////////////////////////////////////////////////////////////////////////
      virtual void encode(System::SerialBuffer& buffer) const;

      // Struct variables
      std::string name; ///< new user name
    };

    //-----------------------------------------------------------------------//
    // Class:       UserMsg
    // Description: Message containing user ID info
    //-----------------------------------------------------------------------//
    struct UserMsg : public System::Serializable
    {
      /////////////////////////////////////////////////////////////////////////
      /// @brief Constructor
      /////////////////////////////////////////////////////////////////////////
      UserMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Destructor
      /////////////////////////////////////////////////////////////////////////
      virtual ~UserMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Decodes the member data from the buffer
      /// @param buffer - buffer to append to
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      virtual bool decode(System::SerialBuffer& buffer);

      /////////////////////////////////////////////////////////////////////////
      /// @brief Encodes the member data into the buffer
      /// @param buffer - buffer to append to
      /// @return - None
      /////////////////////////////////////////////////////////////////////////
      virtual void encode(System::SerialBuffer& buffer) const;

      // Struct variables
      uint32_t    id;   ///< new user ID
      std::string name; ///< new user name
    };

    //-----------------------------------------------------------------------//
    // Class:       GameInfoMsg
    // Description: Message containing game ID info
    //-----------------------------------------------------------------------//
    struct GameInfoMsg : public System::Serializable
    {
      /////////////////////////////////////////////////////////////////////////
      /// @brief Constructor
      /////////////////////////////////////////////////////////////////////////
      GameInfoMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Destructor
      /////////////////////////////////////////////////////////////////////////
      virtual ~GameInfoMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Decodes the member data from the buffer
      /// @param buffer - buffer to append to
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      virtual bool decode(System::SerialBuffer& buffer);

      /////////////////////////////////////////////////////////////////////////
      /// @brief Encodes the member data into the buffer
      /// @param buffer - buffer to append to
      /// @return - None
      /////////////////////////////////////////////////////////////////////////
      virtual void encode(System::SerialBuffer& buffer) const;

      // Struct variables
      uint32_t    id;         ///< game ID
      std::string name;       ///< game name
      size_t      numPlayers; ///< number of players
      GameState   state;      ///< game state
    };

    //-----------------------------------------------------------------------//
    // Class:       OpenGamesMsg
    // Description: Message containing all open games
    //-----------------------------------------------------------------------//
    struct OpenGamesMsg : public System::Serializable
    {
      /////////////////////////////////////////////////////////////////////////
      /// @brief Constructor
      /////////////////////////////////////////////////////////////////////////
      OpenGamesMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Destructor
      /////////////////////////////////////////////////////////////////////////
      virtual ~OpenGamesMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Decodes the member data from the buffer
      /// @param buffer - buffer to append to
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      virtual bool decode(System::SerialBuffer& buffer);

      /////////////////////////////////////////////////////////////////////////
      /// @brief Encodes the member data into the buffer
      /// @param buffer - buffer to append to
      /// @return - None
      /////////////////////////////////////////////////////////////////////////
      virtual void encode(System::SerialBuffer& buffer) const;

      // Struct variables
      std::list<GameInfoMsg> games; ///< open games
    };

    //-----------------------------------------------------------------------//
    // Class:       RequestGameMsg
    // Description: Message for requesting a new game creation
    //-----------------------------------------------------------------------//
    // Contents would be identical to RequestUserMsg
    struct RequestGameMsg : public RequestUserMsg {};

    //-----------------------------------------------------------------------//
    // Class:       RequestJoinGameMsg
    // Description: Message for requesting to join a new game
    //-----------------------------------------------------------------------//
    struct RequestJoinGameMsg : public System::Serializable
    {
      /////////////////////////////////////////////////////////////////////////
      /// @brief Constructor
      /////////////////////////////////////////////////////////////////////////
      RequestJoinGameMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Destructor
      /////////////////////////////////////////////////////////////////////////
      virtual ~RequestJoinGameMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Decodes the member data from the buffer
      /// @param buffer - buffer to append to
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      virtual bool decode(System::SerialBuffer& buffer);

      /////////////////////////////////////////////////////////////////////////
      /// @brief Encodes the member data into the buffer
      /// @param buffer - buffer to append to
      /// @return - None
      /////////////////////////////////////////////////////////////////////////
      virtual void encode(System::SerialBuffer& buffer) const;

      // Struct variables
      uint32_t gameID; ///< game ID requesting to join
    };

    //-----------------------------------------------------------------------//
    // Class:       GameMembersMsg
    // Description: Message containing the members of a game
    //-----------------------------------------------------------------------//
    struct GameMembersMsg : public System::Serializable
    {
      /////////////////////////////////////////////////////////////////////////
      /// @brief Constructor
      /////////////////////////////////////////////////////////////////////////
      GameMembersMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Destructor
      /////////////////////////////////////////////////////////////////////////
      virtual ~GameMembersMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Decodes the member data from the buffer
      /// @param buffer - buffer to append to
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      virtual bool decode(System::SerialBuffer& buffer);

      /////////////////////////////////////////////////////////////////////////
      /// @brief Encodes the member data into the buffer
      /// @param buffer - buffer to append to
      /// @return - None
      /////////////////////////////////////////////////////////////////////////
      virtual void encode(System::SerialBuffer& buffer) const;

      // Struct variables
      uint32_t           gameID;  ///< game ID
      std::list<UserMsg> players; ///< players in game
    };

    //-----------------------------------------------------------------------//
    // Class:       DeckMsg
    // Description: Message containing cards in a deck
    //-----------------------------------------------------------------------//
    struct DeckMsg : public System::Serializable
    {
      /////////////////////////////////////////////////////////////////////////
      /// @brief Constructor
      /////////////////////////////////////////////////////////////////////////
      DeckMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Destructor
      /////////////////////////////////////////////////////////////////////////
      virtual ~DeckMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Decodes the member data from the buffer
      /// @param buffer - buffer to append to
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      virtual bool decode(System::SerialBuffer& buffer);

      /////////////////////////////////////////////////////////////////////////
      /// @brief Encodes the member data into the buffer
      /// @param buffer - buffer to append to
      /// @return - None
      /////////////////////////////////////////////////////////////////////////
      virtual void encode(System::SerialBuffer& buffer) const;

      // Struct variables
      bool            locked; ///< deck locked for editing?
      std::list<Card> cards;  ///< cards in deck
    };

    //-----------------------------------------------------------------------//
    // Class:       PhaseMsg
    // Description: Message containing the player phase data
    //-----------------------------------------------------------------------//
    struct PhaseMsg : public System::Serializable
    {
      /////////////////////////////////////////////////////////////////////////
      /// @brief Constructor
      /////////////////////////////////////////////////////////////////////////
      PhaseMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Destructor
      /////////////////////////////////////////////////////////////////////////
      virtual ~PhaseMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Decodes the member data from the buffer
      /// @param buffer - buffer to append to
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      virtual bool decode(System::SerialBuffer& buffer);

      /////////////////////////////////////////////////////////////////////////
      /// @brief Encodes the member data into the buffer
      /// @param buffer - buffer to append to
      /// @return - None
      /////////////////////////////////////////////////////////////////////////
      virtual void encode(System::SerialBuffer& buffer) const;

      // Struct variables
      std::list<DeckMsg> matches; ///< match decks
    };

    //-----------------------------------------------------------------------//
    // Class:       PlayerStateMsg
    // Description: Message containing the player state data
    //-----------------------------------------------------------------------//
    struct PlayerStateMsg : public System::Serializable
    {
      /////////////////////////////////////////////////////////////////////////
      /// @brief Constructor
      /////////////////////////////////////////////////////////////////////////
      PlayerStateMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Destructor
      /////////////////////////////////////////////////////////////////////////
      virtual ~PlayerStateMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Decodes the member data from the buffer
      /// @param buffer - buffer to append to
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      virtual bool decode(System::SerialBuffer& buffer);

      /////////////////////////////////////////////////////////////////////////
      /// @brief Encodes the member data into the buffer
      /// @param buffer - buffer to append to
      /// @return - None
      /////////////////////////////////////////////////////////////////////////
      virtual void encode(System::SerialBuffer& buffer) const;

      // Struct variables
      uint32_t    playerID; ///< player ID
      size_t      score;    ///< total score
      bool        skipped;  ///< player skipped?
      PlayerState state;    ///< player state
      PhaseNumber phaseNum; ///< phase number
      DeckMsg     hand;     ///< player hand
      PhaseMsg    phase;    ///< phase
    };

    //-----------------------------------------------------------------------//
    // Class:       GameStateMsg
    // Description: Message containing the game state data
    //-----------------------------------------------------------------------//
    struct GameStateMsg : public System::Serializable
    {
      /////////////////////////////////////////////////////////////////////////
      /// @brief Constructor
      /////////////////////////////////////////////////////////////////////////
      GameStateMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Destructor
      /////////////////////////////////////////////////////////////////////////
      virtual ~GameStateMsg() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Decodes the member data from the buffer
      /// @param buffer - buffer to append to
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      virtual bool decode(System::SerialBuffer& buffer);

      /////////////////////////////////////////////////////////////////////////
      /// @brief Encodes the member data into the buffer
      /// @param buffer - buffer to append to
      /// @return - None
      /////////////////////////////////////////////////////////////////////////
      virtual void encode(System::SerialBuffer& buffer) const;

      // Struct variables
      uint32_t  gameID;          ///< game ID
      uint32_t  startPlayerID;   ///< start player ID
      uint32_t  currentPlayerID; ///< current player ID
      uint32_t  winnerID;        ///< winner player ID
      GameState state;           ///< game state
      DeckMsg   drawPile;        ///< draw pile
      DeckMsg   discardPile;     ///< discard pile

      std::list<PlayerStateMsg> players; ///< player states
    };

  } // namespace Messages
} // namespace Phase10
