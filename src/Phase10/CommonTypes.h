/**
 * @file CommonTypes.h
 *
 * @brief Declarations for all Phase 10 common types
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <memory>

namespace Phase10
{
  // Forward declarations
  class Card;
  class ColorSet;
  class Deck;
  class DeckInterface;
  class Game;
  class GameInfo;
  class GameFactory;
  class Match;
  class Phase;
  class Player;
  class Run;
  class State;
  class User;
  class UserFactory;
  class ValueSet;

  // Typedefs
  using sCard          = std::shared_ptr<Card>;
  using sColorSet      = std::shared_ptr<ColorSet>;
  using sDeck          = std::shared_ptr<Deck>;
  using sDeckInterface = std::shared_ptr<DeckInterface>;
  using sGame          = std::shared_ptr<Game>;
  using sGameInfo      = std::shared_ptr<GameInfo>;
  using sGameFactory   = std::shared_ptr<GameFactory>;
  using sMatch         = std::shared_ptr<Match>;
  using sPhase         = std::shared_ptr<Phase>;
  using sPlayer        = std::shared_ptr<Player>;
  using sRun           = std::shared_ptr<Run>;
  using sState         = std::shared_ptr<State>;
  using sUser          = std::shared_ptr<User>;
  using sUserFactory   = std::shared_ptr<UserFactory>;
  using sValueSet      = std::shared_ptr<ValueSet>;

  using scCard          = std::shared_ptr<const Card>;
  using scColorSet      = std::shared_ptr<const ColorSet>;
  using scDeck          = std::shared_ptr<const Deck>;
  using scDeckInterface = std::shared_ptr<const DeckInterface>;
  using scGame          = std::shared_ptr<const Game>;
  using scGameInfo      = std::shared_ptr<const GameInfo>;
  using scsGameFactory  = std::shared_ptr<const sGameFactory>;
  using scMatch         = std::shared_ptr<const Match>;
  using scPhase         = std::shared_ptr<const Phase>;
  using scPlayer        = std::shared_ptr<const Player>;
  using scRun           = std::shared_ptr<const Run>;
  using scState         = std::shared_ptr<const State>;
  using scUser          = std::shared_ptr<const User>;
  using scUserFactory   = std::shared_ptr<const UserFactory>;
  using scValueSet      = std::shared_ptr<const ValueSet>;

  //-------------------------------------------------------------------------//
  // Enum:        CardColor
  // Description: Colors for Phase 10 cards
  //-------------------------------------------------------------------------//
  using CardColor = uint32_t;
  enum
  {
    COLOR_RED,    ///< red cards
    COLOR_BLUE,   ///< blue cards
    COLOR_GREEN,  ///< green cards
    COLOR_YELLOW, ///< yellow cards
    COLOR_NONE    ///< special cards
  };

  //-------------------------------------------------------------------------//
  // Enum:        CardValue
  // Description: Values for Phase 10 cards
  //-------------------------------------------------------------------------//
  using CardValue = uint32_t;
  enum
  {
    VALUE_STD_MIN = 1,  ///< minimum value of standard cards
    VALUE_STD_MAX = 12, ///< maximum value of standard cards

    VALUE_WILD,   ///< wild card
    VALUE_SKIP,   ///< skip card
    VALUE_INVALID ///< invalid card value
  };

  //-------------------------------------------------------------------------//
  // Enum:        DeckMode
  // Description: Mode for managing deck
  //-------------------------------------------------------------------------//
  using DeckMode = uint32_t;
  enum
  {
    MODE_LIFO,       ///< no sorting, insert at back
    MODE_FIFO,       ///< no sorting, insert at front
    MODE_SORT_COLOR, ///< sort by color
    MODE_SORT_VALUE  ///< sort by value
  };

  //-------------------------------------------------------------------------//
  // Enum:        PhaseNumber
  // Description: Phase number
  //-------------------------------------------------------------------------//
  using PhaseNumber = uint32_t;
  enum
  {
    PHASE_FIRST = 1, ///< first phase
    PHASE_LAST  = 10 ///< last phase
  };

  //-------------------------------------------------------------------------//
  // Enum:        PhaseType
  // Description: Phase type
  //-------------------------------------------------------------------------//
  using PhaseType = uint32_t;
  enum
  {
    PHASE_COLOR_SET, ///< set of cards with same color
    PHASE_VALUE_SET, ///< set of cards with same value
    PHASE_RUN        ///< sequence of cards
  };

  //-------------------------------------------------------------------------//
  // Enum:        PlayerState
  // Description: State of player
  //-------------------------------------------------------------------------//
  using PlayerState = uint32_t;
  enum
  {
    PLAYER_INIT,     ///< waiting for deal
    PLAYER_INACTIVE, ///< not my turn
    PLAYER_IDLE,     ///< my turn, waiting to draw
    PLAYER_ACTIVE,   ///< actively playing
    PLAYER_SKIP,     ///< discarded skip card
    PLAYER_OUT,      ///< out of cards
    PLAYER_QUIT      ///< player has quit the game
  };

  //-------------------------------------------------------------------------//
  // Enum:        GameState
  // Description: State of game
  //-------------------------------------------------------------------------//
  using GameState = uint32_t;
  enum
  {
    GAME_INIT,      ///< initial state (gathering players)
    GAME_ACTIVE,    ///< round currently active
    GAME_ROUND_END, ///< end of round, waiting for next deal
    GAME_OVER,      ///< game complete
    GAME_QUIT       ///< too many players quit the game
  };

  //-------------------------------------------------------------------------//
  // Enum:        MessageID
  // Description: Message ID for traffic between games
  //-------------------------------------------------------------------------//
  using MessageID = uint32_t;
  enum
  {
    MSG_REQUEST_USER, ///< requesting user ID for new user
    MSG_NEW_USER,     ///< new user created

    MSG_OPEN_GAMES,          ///< message containing currently existing games
    MSG_REQUEST_GAME_CREATE, ///< request creation of new game
    MSG_REQUEST_GAME_JOIN,   ///< request user to join an existing game
    MSG_GAME_MEMBERS,        ///< update to members of game

    MSG_REQUEST_DEAL, ///< request that the next round be dealt
    MSG_GAME_UPDATE,  ///< game state update
    MSG_PLAYER_UPDATE ///< player state update
  };

  namespace Messages
  {
    // Forward declarations
    struct DeckMsg;
    struct GameInfoMsg;
    struct GameMembersMsg;
    struct GameStateMsg;
    struct OpenGamesMsg;
    struct PhaseMsg;
    struct PlayerStateMsg;
    struct RequestGameMsg;
    struct RequestJoinGameMsg;
    struct RequestUserMsg;
    struct UserMsg;

    // Typedefs
    using sDeckMsg            = std::shared_ptr<DeckMsg>;
    using sGameInfoMsg        = std::shared_ptr<GameInfoMsg>;
    using sGameMembersMsg     = std::shared_ptr<GameMembersMsg>;
    using sGameStateMsg       = std::shared_ptr<GameStateMsg>;
    using sOpenGamesMsg       = std::shared_ptr<OpenGamesMsg>;
    using sPhaseMsg           = std::shared_ptr<PhaseMsg>;
    using sPlayerStateMsg     = std::shared_ptr<PlayerStateMsg>;
    using sRequestGameMsg     = std::shared_ptr<RequestGameMsg>;
    using sRequestJoinGameMsg = std::shared_ptr<RequestJoinGameMsg>;
    using sRequestUserMsg     = std::shared_ptr<RequestUserMsg>;
    using sUserMsg            = std::shared_ptr<UserMsg>;

    using scDeckMsg            = std::shared_ptr<const DeckMsg>;
    using scGameInfoMsg        = std::shared_ptr<const GameInfoMsg>;
    using scGameMembersMsg     = std::shared_ptr<const GameMembersMsg>;
    using scGameStateMsg       = std::shared_ptr<const GameStateMsg>;
    using scOpenGamesMsg       = std::shared_ptr<const OpenGamesMsg>;
    using scPhaseMsg           = std::shared_ptr<const PhaseMsg>;
    using scPlayerStateMsg     = std::shared_ptr<const PlayerStateMsg>;
    using scRequestGameMsg     = std::shared_ptr<const RequestGameMsg>;
    using scRequestJoinGameMsg = std::shared_ptr<const RequestJoinGameMsg>;
    using scRequestUserMsg     = std::shared_ptr<const RequestUserMsg>;
    using scUserMsg            = std::shared_ptr<const UserMsg>;

  } // namespace Messages

} // namespace Phase10
