/**
 * @file Phase10ColorSet.h
 *
 * @brief Classes to represent set of same color
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <Phase10/Match.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       ColorSet
  // Description: Set of cards with matching color
  //-------------------------------------------------------------------------//
  class ColorSet : public Match
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param minSize - minimum size
    ///////////////////////////////////////////////////////////////////////////
    ColorSet(size_t minSize);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~ColorSet() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Determines if the match is complete
    /// @return - true if complete, else false
    ///////////////////////////////////////////////////////////////////////////
    virtual bool isComplete() const;

  protected:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Check if card can be added
    /// @param card - card to add
    /// @param front - add to front?
    /// @return - true if passed, else false
    ///////////////////////////////////////////////////////////////////////////
    virtual bool canAdd(const Card& card, bool front) const;

  private:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the color of the set
    /// @return - card color, or none if none yet
    ///////////////////////////////////////////////////////////////////////////
    CardColor getColor() const;
  };

} // namespace Phase10
