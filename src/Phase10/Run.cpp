/**
 * @file Phase10Run.cpp
 *
 * @brief Classes to represent run of cards
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Phase10/Card.h>
#include <Phase10/Deck.h>
#include <Phase10/Run.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Run::Run(size_t minSize) : Match(minSize, "RUN OF " + std::to_string(minSize))
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // isComplete
  /////////////////////////////////////////////////////////////////////////////
  bool Run::isComplete() const
  {
    // Must have at least 1 non-wild card to be done
    return(Match::isComplete() && getFirstValue() != VALUE_WILD);
  }

  /////////////////////////////////////////////////////////////////////////////
  // canAdd
  /////////////////////////////////////////////////////////////////////////////
  bool Run::canAdd(const Card& card, bool front) const
  {
    // Get range of acceptable values
    CardValue minValue = VALUE_STD_MAX + 1;
    CardValue maxValue = VALUE_STD_MIN - 1;

    if(front)
    {
      CardValue start = getFirstValue();
      if(start == VALUE_WILD)
      {
        minValue = VALUE_STD_MIN;
        maxValue = VALUE_STD_MAX - Match::getDeck()->size();
      }
      else if(start > VALUE_STD_MIN)
      {
        minValue = start - 1;
        maxValue = minValue;
      }
    }
    else
    {
      CardValue last = getLastValue();
      if(last == VALUE_WILD)
      {
        minValue = VALUE_STD_MIN + Match::getDeck()->size();
        maxValue = VALUE_STD_MAX;
      }
      else if(last < VALUE_STD_MAX)
      {
        minValue = last + 1;
        maxValue = minValue;
      }
    }

    // Check if card can be added
    // 1 - standard card in range
    // 2 - wild card with any range
    return(maxValue >= minValue && (card.isWild() || (card.getValue() >= minValue && card.getValue() <= maxValue)));
  }

  /////////////////////////////////////////////////////////////////////////////
  // getFirstValue
  /////////////////////////////////////////////////////////////////////////////
  CardValue Run::getFirstValue() const
  {
    // Assumption: Cards are already a valid run
    uint32_t numWilds = 0;
    auto& deck = *Match::getDeck();
    for(auto& card : deck)
    {
      if(card.isStandard())
      {
        return(card.getValue() - numWilds);
      }
      else if(card.isWild())
      {
        ++numWilds;
      }
    }

    // No non-wild cards
    return VALUE_WILD;
  }

  /////////////////////////////////////////////////////////////////////////////
  // getLastValue
  /////////////////////////////////////////////////////////////////////////////
  CardValue Run::getLastValue() const
  {
    // Assumption: Cards are already a valid run
    uint32_t numWilds = 0;
    auto& deck = *Match::getDeck();
    for(auto it = deck.rbegin(); it != deck.rend(); ++it)
    {
      auto& card = *it;
      if(card.isStandard())
      {
        return(card.getValue() + numWilds);
      }
      else if(card.isWild())
      {
        ++numWilds;
      }
    }

    // No non-wild cards
    return VALUE_WILD;
  }

} // namespace Phase10
