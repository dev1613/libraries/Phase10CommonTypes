/**
 * @file Game.h
 *
 * @brief Represents a unique game for the Phase 10 system
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <cstdint>
#include <list>
#include <string>

#include <Common/Factory.h>
#include <Common/ID_Factory.h>
#include <Phase10/CommonTypes.h>
#include <Phase10/Deck.h>

#include <System/Logger.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       GameInfo
  // Description: Holds the identifying information for a game
  //-------------------------------------------------------------------------//
  class GameInfo
  {
  public:
    // Constants
    static constexpr size_t MIN_PLAYERS = 2;
    static constexpr size_t MAX_PLAYERS = 6;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param id - game ID
    /// @param name - game name
    /// @param numPlayers - number of players
    /// @param initState - initial state
    ///////////////////////////////////////////////////////////////////////////
    GameInfo(uint32_t id, const std::string& name, size_t numPlayers = 0, GameState initState = GAME_INIT) :
      mID(id), mName(name), mNumPlayers(numPlayers), mState(initState)
    {
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~GameInfo() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Checks if new players can be added
    /// @return - true if players can be added, else false
    ///////////////////////////////////////////////////////////////////////////
    bool canAddPlayers() const { return(!full() && mState == GAME_INIT); }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the game ID
    /// @return - game ID
    ///////////////////////////////////////////////////////////////////////////
    uint32_t id() const { return mID; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Determines if the game is full
    /// @return - true if full, else false
    ///////////////////////////////////////////////////////////////////////////
    bool full() const { return(mNumPlayers >= MAX_PLAYERS); }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the game name
    /// @return - game name
    ///////////////////////////////////////////////////////////////////////////
    const std::string& name() const { return mName; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the number of players
    /// @return - number of players
    ///////////////////////////////////////////////////////////////////////////
    size_t numPlayers() const { return mNumPlayers; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Determines if the game is ready to start
    /// @return - true if ready, else false
    ///////////////////////////////////////////////////////////////////////////
    bool ready() const { return(mNumPlayers >= MIN_PLAYERS && mNumPlayers <= MAX_PLAYERS); }

  private:
    friend class Game;

    // Private class variables
    const uint32_t    mID;   ///< game ID
    const std::string mName; ///< game name

    size_t    mNumPlayers; ///< number of players
    GameState mState;      ///< game state
  };

  //-------------------------------------------------------------------------//
  // Class:       Game
  // Description: Represents a unique Phase 10 game
  //-------------------------------------------------------------------------//
  class Game
  {
  public:
    // Public types
    using DeckListener   = typename Deck::Listener;
    using PlayerList     = std::list<sPlayer>;
    using PlayerListener = std::function<void(sPlayer)>;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~Game() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Adds a player
    /// @param pUser - user to join
    /// @return - player added, or nullptr if failed
    ///////////////////////////////////////////////////////////////////////////
    sPlayer addPlayer(const User* pUser);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Adds a player listener
    /// @param listener - function to call when player state is updated
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void addPlayerListener(const PlayerListener& listener);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Adds a listener for updates to the draw pile
    /// @param listener - new listener to add
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void addDrawPileListener(const DeckListener& listener);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Adds a listener for updates to the discard pile
    /// @param listener - new listener to add
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void addDiscardPileListener(const DeckListener& listener);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the top card of the discard pile
    /// @param card[out] - top card
    /// @return - true if card is valid, else false
    ///////////////////////////////////////////////////////////////////////////
    bool getDiscardPileTopCard(Card& card) const { return mDiscardPile->peek(card); }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the game info data
    /// @param msg[out] - game info
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void getInfo(Messages::GameInfoMsg& msg) const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the game members
    /// @param msg[out] - game members
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void getMembers(Messages::GameMembersMsg& msg) const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets a player
    /// @param playerID - player ID to find
    /// @return - player with ID
    ///////////////////////////////////////////////////////////////////////////
    sPlayer getPlayer(uint32_t playerID) const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets all the players
    /// @return - list of players
    ///////////////////////////////////////////////////////////////////////////
    const PlayerList& getPlayers() const { return mPlayers; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the game state data
    /// @param msg[out] - game state data
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void getState(Messages::GameStateMsg& msg) const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the game ID
    /// @return - game ID
    ///////////////////////////////////////////////////////////////////////////
    uint32_t id() const { return mGameInfo.id(); }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the game info
    /// @return - game info
    ///////////////////////////////////////////////////////////////////////////
    const GameInfo& info() const { return mGameInfo; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Determines if the game is full
    /// @return - true if full, else false
    ///////////////////////////////////////////////////////////////////////////
    bool full() const { return mGameInfo.full(); }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the game name
    /// @return - game name
    ///////////////////////////////////////////////////////////////////////////
    const std::string& name() const { return mGameInfo.name(); }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the number of cards in the draw pile
    /// @return - number of cards
    ///////////////////////////////////////////////////////////////////////////
    size_t numCardsInDrawPile() const { return mDrawPile->size(); }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the number of players
    /// @return - number of players
    ///////////////////////////////////////////////////////////////////////////
    size_t numPlayers() const { return mGameInfo.numPlayers(); }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Determines if the game is ready to start
    /// @return - true if ready, else false
    ///////////////////////////////////////////////////////////////////////////
    bool ready() const { return mGameInfo.ready(); }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Removes a player from the game
    /// @param userID - user ID of the player to remove
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void removePlayer(uint32_t userID);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sets the game members
    /// @param msg - game members
    /// @param userFactory - factory to add users if needed
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void setMembers(const Messages::GameMembersMsg& msg, UserFactory& userFactory);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sets the game state data
    /// @param msg - game state data
    /// @param playerAdvanced[out] - was player advanced?
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void setState(const Messages::GameStateMsg& msg);
    void setState(const Messages::GameStateMsg& msg, bool& playerAdvanced);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Starts the next round
    /// @return - true if successfully dealt, else false
    ///////////////////////////////////////////////////////////////////////////
    bool startRound();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the game state
    /// @return - game state
    ///////////////////////////////////////////////////////////////////////////
    GameState state() const { return mGameInfo.mState; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the player who won the game
    /// @return - winning player, or nullptr if game is not over
    ///////////////////////////////////////////////////////////////////////////
    scPlayer winner() const { return mWinner; }

  private:
    friend class Common::Factory<Game>;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param id - game ID
    /// @param name - game name
    /// @param master - true for master game, else false
    ///////////////////////////////////////////////////////////////////////////
    Game(System::sLogger pLogger, uint32_t id, const std::string& name, bool master = true);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Finds a player
    /// @param pPlayer/id - player to find
    /// @return - iterator for player
    ///////////////////////////////////////////////////////////////////////////
    PlayerList::iterator findPlayer(sPlayer pPlayer);
    PlayerList::iterator findPlayer(uint32_t id);
    PlayerList::const_iterator findPlayer(uint32_t id) const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the next player
    /// @param player - current player iterator
    /// @return - next player iterator
    ///////////////////////////////////////////////////////////////////////////
    PlayerList::iterator nextPlayer(PlayerList::iterator player);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Called when a player's state is updated
    /// @param pPlayer - player whose state was updated
    /// @param notify - notify player listeners?
    /// @param playerAdvanced - true if player advanced
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void playerUpdateCallback(sPlayer pPlayer, bool notify);
    void playerUpdate(sPlayer pPlayer, bool notify, bool& playerAdvanced);

    // Private class variables
    const System::sLogger mLogger; ///< debug logger
    const bool            mMaster; ///< master game?

    PlayerList mPlayers;     ///< current players
    sDeck      mDrawPile;    ///< draw pile
    sDeck      mDiscardPile; ///< discard pile
    GameInfo   mGameInfo;    ///< game info

    sPlayer mStartPlayer;   ///< player who starts the next round
    sPlayer mCurrentPlayer; ///< current player
    sPlayer mWinner;        ///< winner player (only valid in GAME_OVER)

    bool                      mMsgUpdate;       ///< message update in progress?
    std::list<PlayerListener> mPlayerListeners; ///< listeners for player state updates
  };

  //-------------------------------------------------------------------------//
  // Class:       GameFactory
  // Description: Singleton factory of games
  //-------------------------------------------------------------------------//
  class GameFactory : public Common::Factory<Game>
  {
  public:
    using Base = Common::Factory<Game>;

    // Constants
    static constexpr uint32_t INVALID_ID = 0;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param master - true for creating master instances
    ///////////////////////////////////////////////////////////////////////////
    GameFactory(System::sLogger pLogger, bool master = true);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~GameFactory() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Creates a game
    /// @param id - game ID (if not provided, gets next available)
    /// @param name - game name
    /// @param master - game master?
    /// @return - new game, or nullptr if failed
    ///////////////////////////////////////////////////////////////////////////
    sGame create(uint32_t id, const std::string& name);
    sGame create(const std::string& name);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Finds a game in the factory
    /// @param id - game ID
    /// @return - game with the ID, or nullptr if not found
    ///////////////////////////////////////////////////////////////////////////
    sGame find(uint32_t id);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Finds a game in the factory or creates if it doesn't exist
    /// @param id - game ID
    /// @param name - game name
    /// @return - game with the ID, or new game if not found
    ///////////////////////////////////////////////////////////////////////////
    sGame findOrCreate(uint32_t id, const std::string& name);

  private:
    // Private class variables
    const bool            mMaster; ///< set whether factory creates master games
    const System::sLogger mLogger; ///< debug logger

    Common::ID_Factory<uint32_t> mIDs;    ///< ID factory
  };

} // namespace Phase10
