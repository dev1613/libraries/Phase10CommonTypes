/**
 * @file Phase10ColorSet.cpp
 *
 * @brief Classes to represent set of same color
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Phase10/Card.h>
#include <Phase10/ColorSet.h>
#include <Phase10/Deck.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  ColorSet::ColorSet(size_t minSize) : Match(minSize, std::to_string(minSize) + " CARDS OF 1 COLOR")
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // isComplete
  /////////////////////////////////////////////////////////////////////////////
  bool ColorSet::isComplete() const
  {
    // Must have at least 1 non-wild card to be done
    return(Match::isComplete() && getColor() != COLOR_NONE);
  }

  /////////////////////////////////////////////////////////////////////////////
  // canAdd
  /////////////////////////////////////////////////////////////////////////////
  bool ColorSet::canAdd(const Card& card, bool front) const
  {
    // Direction doesn't matter for sets
    // 1 - Can add any wild card
    // 2 - Can add any standard card that matches the set color
    CardColor setColor = getColor();
    return(card.isWild() ||
           (card.isStandard() && 
             (setColor == card.getColor() || setColor == COLOR_NONE)));
  }

  /////////////////////////////////////////////////////////////////////////////
  // getColor
  /////////////////////////////////////////////////////////////////////////////
  CardColor ColorSet::getColor() const
  {
    // Assumption: All standard cards should match
    auto& deck = *Match::getDeck();
    for(auto& card : deck)
    {
      if(card.isStandard())
      {
        return card.getColor();
      }
    }

    // No non-wild cards
    return COLOR_NONE;
  }

} // namespace Phase10
