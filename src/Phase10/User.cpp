/**
 * @file User.cpp
 *
 * @brief Represents a unique user for the Phase 10 system
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Phase10/Game.h>
#include <Phase10/User.h>

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  User::User(System::sLogger pLogger, uint32_t id, const std::string& name) :
      mLogger(pLogger), mID(id), mName(name)
  {
    mGame = nullptr;
  }

  /////////////////////////////////////////////////////////////////////////////
  // join
  /////////////////////////////////////////////////////////////////////////////
  bool User::join(Game* pGame)
  {
    bool success = false;
    if(mGame == nullptr && pGame->addPlayer(this))
    {
      mGame = pGame;
      success = true;
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // leave
  /////////////////////////////////////////////////////////////////////////////
  bool User::leave()
  {
    bool success = false;
    if(mGame)
    {
      mGame->removePlayer(mID);
      mGame = nullptr;
      success = true;
    }

    return success;
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  UserFactory::UserFactory(System::sLogger pLogger) : mLogger(pLogger)
  {
    mIDs.take(INVALID_ID);
  }

  /////////////////////////////////////////////////////////////////////////////
  // create
  /////////////////////////////////////////////////////////////////////////////
  std::shared_ptr<User> UserFactory::create(uint32_t id, const std::string& name)
  {
    std::shared_ptr<User> pUser;
    if(mIDs.take(id))
    {
      pUser = Base::create(mLogger, id, name);
    }

    return pUser;
  }

  /////////////////////////////////////////////////////////////////////////////
  // create
  /////////////////////////////////////////////////////////////////////////////
  std::shared_ptr<User> UserFactory::create(const std::string& name)
  {
    std::shared_ptr<User> pUser;
    uint32_t id = 0;
    if(mIDs.takeNext(id))
    {
      pUser = Base::create(mLogger, id, name);
    }

    return pUser;
  }

  /////////////////////////////////////////////////////////////////////////////
  // find
  /////////////////////////////////////////////////////////////////////////////
  sUser UserFactory::find(uint32_t id)
  {
    return Base::find([id](const User& user) { return(user.id() == id); });
  }

  /////////////////////////////////////////////////////////////////////////////
  // findOrCreate
  /////////////////////////////////////////////////////////////////////////////
  sUser UserFactory::findOrCreate(uint32_t id, const std::string& name)
  {
    auto pUser = find(id);
    if(pUser == nullptr)
    {
      pUser = create(id, name);
    }

    return pUser;
  }

} // namespace Phase10
