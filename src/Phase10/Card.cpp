/**
 * @file Phase10Card.cpp
 *
 * @brief Classes to represent Phase 10 cards
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <map>
#include <sstream>

#include <Phase10/Card.h>
#include <System/SerialBuffer.h>

namespace Phase10
{
  // Constants
  static const std::string DEFAULT_COLOR("NONE");
  static const std::map<CardColor, std::string> COLORS({
    { COLOR_RED, "Red" },
    { COLOR_BLUE, "Blue" },
    { COLOR_GREEN, "Green" },
    { COLOR_YELLOW, "Yellow" }
  });

  /////////////////////////////////////////////////////////////////////////////
  // ColorToString
  /////////////////////////////////////////////////////////////////////////////
  const std::string& ColorToString(CardColor color)
  {
    // Get color
    auto it = COLORS.find(color);
    if(it != COLORS.end())
    {
      return it->second;
    }
    else
    {
      return DEFAULT_COLOR;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Card::Card(CardValue value, CardColor color) : mValue(value), mColor(color)
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // operator==
  /////////////////////////////////////////////////////////////////////////////
  bool Card::operator==(const Card& other) const
  {
    // Color is irrelevant for skips and wilds
    return(mValue == other.mValue && (!isStandard() || mColor == other.mColor));
  }

  /////////////////////////////////////////////////////////////////////////////
  // operator!=
  /////////////////////////////////////////////////////////////////////////////
  bool Card::operator!=(const Card& other) const
  {
    return !(*this == other);
  }

  /////////////////////////////////////////////////////////////////////////////
  // operator>
  /////////////////////////////////////////////////////////////////////////////
  bool Card::operator>(const Card& other) const
  {
    return(mValue > other.mValue || (mValue == other.mValue && mColor > other.mColor));
  }

  /////////////////////////////////////////////////////////////////////////////
  // operator>=
  /////////////////////////////////////////////////////////////////////////////
  bool Card::operator>=(const Card& other) const
  {
    return(*this > other || *this == other);
  }

  /////////////////////////////////////////////////////////////////////////////
  // operator<
  /////////////////////////////////////////////////////////////////////////////
  bool Card::operator<(const Card& other) const
  {
    return(mValue < other.mValue || (mValue == other.mValue && mColor < other.mColor));
  }

  /////////////////////////////////////////////////////////////////////////////
  // operator<=
  /////////////////////////////////////////////////////////////////////////////
  bool Card::operator<=(const Card& other) const
  {
    return(*this < other || *this == other);
  }

  /////////////////////////////////////////////////////////////////////////////
  // decode
  /////////////////////////////////////////////////////////////////////////////
  bool Card::decode(System::SerialBuffer& buffer)
  {
    Card backup(*this);
    bool valid = true;

    valid = valid && buffer.read(mColor);
    valid = valid && buffer.read(mValue);

    if(!valid)
    {
      *this = backup;
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // encode
  /////////////////////////////////////////////////////////////////////////////
  void Card::encode(System::SerialBuffer& buffer) const
  {
    buffer.append(mColor);
    buffer.append(mValue);
  }

  /////////////////////////////////////////////////////////////////////////////
  // getPoints
  /////////////////////////////////////////////////////////////////////////////
  size_t Card::getPoints() const
  {
    // Constants
    static constexpr size_t HIGH_CARD_POINTS    = 10;
    static constexpr size_t HIGH_CARD_VALUE     = 10;
    static constexpr size_t INVALID_CARD_POINTS = 1000;
    static constexpr size_t LOW_CARD_POINTS     = 5;
    static constexpr size_t SPECIAL_CARD_POINTS = 25;

    // Get points
    if(isSkip() || isWild())
    {
      return SPECIAL_CARD_POINTS;
    }
    else if(!isStandard())
    {
      return INVALID_CARD_POINTS;
    }
    else if(mValue >= HIGH_CARD_VALUE)
    {
      return HIGH_CARD_POINTS;
    }
    else
    {
      return LOW_CARD_POINTS;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // isSkip
  /////////////////////////////////////////////////////////////////////////////
  bool Card::isSkip() const
  {
    return(mValue == VALUE_SKIP);
  }

  /////////////////////////////////////////////////////////////////////////////
  // isStandard
  /////////////////////////////////////////////////////////////////////////////
  bool Card::isStandard() const
  {
    return(mValue >= VALUE_STD_MIN && mValue <= VALUE_STD_MAX && COLORS.find(mColor) != COLORS.end());
  }

  /////////////////////////////////////////////////////////////////////////////
  // isValid
  /////////////////////////////////////////////////////////////////////////////
  bool Card::isValid() const
  {
    return(isStandard() || isSkip() || isWild());
  }

  /////////////////////////////////////////////////////////////////////////////
  // isWild
  /////////////////////////////////////////////////////////////////////////////
  bool Card::isWild() const
  {
    return(mValue == VALUE_WILD);
  }

  /////////////////////////////////////////////////////////////////////////////
  // toString
  /////////////////////////////////////////////////////////////////////////////
  std::string Card::toString() const
  {
    // Constants
    static const std::string INVALID("INVALID");
    static const std::string SKIP("Skip");
    static const std::string WILD("Wild");

    // Get card string
    if(isStandard())
    {
      return(ColorToString(mColor) + std::to_string(mValue));
    }
    else if(isSkip())
    {
      return SKIP;
    }
    else if(isWild())
    {
      return WILD;
    }
    else
    {
      return INVALID;
    }
  }

} // namespace Phase10
