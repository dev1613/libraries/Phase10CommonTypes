/**
 * @file User.h
 *
 * @brief Represents a unique user for the Phase 10 system
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <cstdint>
#include <string>

#include <Common/Factory.h>
#include <Common/ID_Factory.h>
#include <Phase10/CommonTypes.h>
#include <System/Logger.h>

namespace Phase10
{
  //-------------------------------------------------------------------------//
  // Class:       User
  // Description: Represents a unique Phase 10 user
  //-------------------------------------------------------------------------//
  class User
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~User() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the current game the user has joined, if any
    /// @return - current game
    ///////////////////////////////////////////////////////////////////////////
    Game* game() const { return mGame; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Joins the request game if not in an active game
    /// @param pGame - game to join
    /// @return - true if joined, else false
    ///////////////////////////////////////////////////////////////////////////
    bool join(Game* pGame);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the user ID
    /// @return - user ID
    ///////////////////////////////////////////////////////////////////////////
    uint32_t id() const { return mID; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Leaves the request game
    /// @return - true if successful, else false
    ///////////////////////////////////////////////////////////////////////////
    bool leave();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the user name
    /// @return - user name
    ///////////////////////////////////////////////////////////////////////////
    const std::string& name() const { return mName; }

  private:
    friend class Common::Factory<User>;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    /// @param id - user ID
    /// @param name - user name
    ///////////////////////////////////////////////////////////////////////////
    User(System::sLogger pLogger, uint32_t id, const std::string& name);

    // Private class variables
    const System::sLogger mLogger; ///< debug logger
    const uint32_t        mID;     ///< user ID
    const std::string     mName;   ///< user name

    Game *mGame; ///< current game, or nullptr if dormant
  };

  //-------------------------------------------------------------------------//
  // Class:       UserFactory
  // Description: Singleton factory of users
  //-------------------------------------------------------------------------//
  class UserFactory : public Common::Factory<User>
  {
  public:
    using Base = Common::Factory<User>;

    // Constants
    static constexpr uint32_t INVALID_ID = 0;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param pLogger - debug logger
    ///////////////////////////////////////////////////////////////////////////
    UserFactory(System::sLogger pLogger);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~UserFactory() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Creates a user
    /// @param id - user ID (if not provided, gets next available)
    /// @param name - user name
    /// @return - new user
    ///////////////////////////////////////////////////////////////////////////
    sUser create(uint32_t id, const std::string& name);
    sUser create(const std::string& name);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Finds a user in the factory
    /// @param id - user ID
    /// @return - user with the ID, or nullptr if not found
    ///////////////////////////////////////////////////////////////////////////
    sUser find(uint32_t id);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Finds a user in the factory or creates if it doesn't exist
    /// @param id - user ID
    /// @param name - user name
    /// @return - user with the ID, or new user if not found
    ///////////////////////////////////////////////////////////////////////////
    sUser findOrCreate(uint32_t id, const std::string& name);

  private:
    // Private class variables
    const System::sLogger mLogger; ///< debug logger

    Common::ID_Factory<uint32_t> mIDs;    ///< ID factory
  };

} // namespace Phase10
