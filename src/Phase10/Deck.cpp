/**
 * @file Phase10Deck.cpp
 *
 * @brief Classes to represent deck of Phase 10 cards
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <algorithm>
#include <mutex>
#include <random>

#include <Phase10/Deck.h>
#include <Phase10/Messages.h>

// Shared random number generator
static std::random_device RD;
static std::mt19937 RANDOM(RD());
static std::mutex RANDOM_MUTEX;

namespace Phase10
{
  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Deck::Deck(const DeckParams& cards, DeckMode mode) : mLocked(false), mMode(mode)
  {
    // Add initial cards to deck
    for(auto& param : cards)
    {
      for(size_t i = 0; i < param.count; ++i)
      {
        // Add without sorting
        addCard(param.card, false);
      }
    }

    // Sort at the end
    sortCards();
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Deck::Deck(DeckMode mode) : mLocked(false), mMode(mode)
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // add (single)
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::add(const Card& card)
  {
    // Add and sort
    bool valid = !mLocked;
    if(valid)
    {
      addCard(card, true);
      notify();
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // add (multiple)
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::add(const CardList& cards)
  {
    bool valid = !mLocked;
    if(valid)
    {
      // Add cards without sorting
      for(auto& card : cards)
      {
        addCard(card, false);
      }

      // Sort at the end
      sortCards();
      notify();
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // addListener
  /////////////////////////////////////////////////////////////////////////////
  void Deck::addListener(const Listener& listener)
  {
    mListeners.push_back(listener);
  }

  /////////////////////////////////////////////////////////////////////////////
  // clear
  /////////////////////////////////////////////////////////////////////////////
  void Deck::clear()
  {
    if(!mLocked)
    {
      mCards.clear();
      notify();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // empty
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::empty() const
  {
    // More efficient than mCards.empty() which checks the size
    return(mCards.begin() == mCards.end());
  }

  /////////////////////////////////////////////////////////////////////////////
  // getCards
  /////////////////////////////////////////////////////////////////////////////
  void Deck::getCards(Messages::DeckMsg& msg) const
  {
    msg.locked = mLocked;
    msg.cards  = mCards;
  }

  /////////////////////////////////////////////////////////////////////////////
  // getPoints
  /////////////////////////////////////////////////////////////////////////////
  size_t Deck::getPoints() const
  {
    // Total all cards in the deck
    size_t points = 0;
    for(auto& card : mCards)
    {
      points += card.getPoints();
    }

    return points;
  }

  /////////////////////////////////////////////////////////////////////////////
  // isSortingEnabled
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::isSortingEnabled() const
  {
    return(mMode == MODE_SORT_COLOR || mMode == MODE_SORT_VALUE);
  }

  /////////////////////////////////////////////////////////////////////////////
  // passAll
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::passAll(Deck& other)
  {
    bool valid = !mLocked && other.add(mCards);
    if(valid)
    {
      mCards.clear();
      notify();
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // passTo
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::passTo(Deck& other)
  {
    Card card;
    bool valid = !other.mLocked && popCard(card);
    if(valid)
    {
      other.addCard(card, true);
      notify();
      other.notify();
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // passTo
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::passTo(const Card& card, Deck& other)
  {
    bool valid = !other.mLocked && removeCard(card);
    if(valid)
    {
      other.addCard(card, true);
      notify();
      other.notify();
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // peek
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::peek(Card& card) const
  {
    bool valid = !empty();
    if(valid)
    {
      card = mCards.front();
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // pop
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::pop()
  {
    Card card;
    return pop(card);
  }

  /////////////////////////////////////////////////////////////////////////////
  // pop
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::pop(Card& card)
  {
    bool valid = popCard(card);
    if(valid)
    {
      notify();
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // remove
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::remove(const Card& card)
  {
    bool valid = removeCard(card);
    if(valid)
    {
      notify();
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // setCards
  /////////////////////////////////////////////////////////////////////////////
  void Deck::setCards(const Messages::DeckMsg& msg)
  {
    mLocked = msg.locked;
    mCards  = msg.cards;
    sortCards();
    notify();
  }

  /////////////////////////////////////////////////////////////////////////////
  // setCards
  /////////////////////////////////////////////////////////////////////////////
  void Deck::setCards(const Deck& other)
  {
    mLocked = other.mLocked;
    mCards  = other.mCards;
    mMode   = other.mMode;
    sortCards();
    notify();
  }

  /////////////////////////////////////////////////////////////////////////////
  // setMode
  /////////////////////////////////////////////////////////////////////////////
  void Deck::setMode(DeckMode mode)
  {
    if(mMode != mode)
    {
      mMode = mode;
      sortCards();
      notify();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // shuffle
  /////////////////////////////////////////////////////////////////////////////
  void Deck::shuffle()
  {
    if(!isSortingEnabled())
    {
      std::vector<std::reference_wrapper<const Card>> vCards(mCards.begin(), mCards.end());
      {
        std::lock_guard<std::mutex> theLock(RANDOM_MUTEX);
        std::shuffle(vCards.begin(), vCards.end(), RANDOM);
      }
      std::list<Card> tmp(vCards.begin(), vCards.end());
      mCards = std::move(tmp);
      notify();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // addCard
  /////////////////////////////////////////////////////////////////////////////
  void Deck::addCard(const Card& card, bool sort)
  {
    // Default insertion mode is LIFO
    if(mMode == MODE_FIFO)
    {
      mCards.push_front(card);
    }
    else
    {
      mCards.push_back(card);
    }

    // Sort if needed
    if(sort)
    {
      sortCards();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // notify
  /////////////////////////////////////////////////////////////////////////////
  void Deck::notify()
  {
    for(auto& listener : mListeners)
    {
      listener();
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // popCard
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::popCard(Card& card)
  {
    bool valid = !mLocked && peek(card);
    if(valid)
    {
      mCards.pop_front();
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // removeCard
  /////////////////////////////////////////////////////////////////////////////
  bool Deck::removeCard(const Card& card)
  {
    bool valid = !mLocked;
    if(valid)
    {
      auto it = std::find(mCards.begin(), mCards.end(), card);
      valid   = (it != mCards.end());
      if(valid)
      {
        mCards.erase(it);
      }
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // sortCards
  /////////////////////////////////////////////////////////////////////////////
  void Deck::sortCards()
  {
    // TODO: Come up with some way to allow reordering the wilds
    // Option 1: Manually move them, then keep in original place when sorting
    // Option 2: Add simulated value for wild cards, then sort based on the simulated value

    // NOTE: Comparators return true if lhs should precede rhs

    // Function to sort by color
    static const auto SORT_BY_COLOR = [](const Card& lhs, const Card& rhs)
    {
      return(lhs.getColor() < rhs.getColor() ||
        (lhs.getColor() == rhs.getColor() && lhs.getValue() < rhs.getValue()));
    };

    // Function to sort by value
    static const auto SORT_BY_VALUE = std::less<Card>();

    // Sort cards
    switch(mMode)
    {
      case MODE_SORT_COLOR:
        mCards.sort(SORT_BY_COLOR);
        break;

      case MODE_SORT_VALUE:
        mCards.sort(SORT_BY_VALUE);
        break;

      default:
        break;
    }
  }

} // namespace Phase10
