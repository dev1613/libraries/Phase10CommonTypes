/**
 * @file Messages.cpp
 *
 * @brief Declarations for Phase 10 messages
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <Phase10/Messages.h>
#include <System/SerialBuffer.h>

namespace Phase10
{
  namespace Messages
  {
    ///////////////////////////////////////////////////////////////////////////
    // decode (RequestUserMsg)
    ///////////////////////////////////////////////////////////////////////////
    bool RequestUserMsg::decode(System::SerialBuffer& buffer)
    {
      bool valid = true;
      valid = valid && buffer.read(name);
      return valid;
    }

    ///////////////////////////////////////////////////////////////////////////
    // encode (RequestUserMsg)
    ///////////////////////////////////////////////////////////////////////////
    void RequestUserMsg::encode(System::SerialBuffer& buffer) const
    {
      buffer.append(name);
    }

    ///////////////////////////////////////////////////////////////////////////
    // decode (UserMsg)
    ///////////////////////////////////////////////////////////////////////////
    bool UserMsg::decode(System::SerialBuffer& buffer)
    {
      bool valid = true;
      valid = valid && buffer.read(id);
      valid = valid && buffer.read(name);
      return valid;
    }

    ///////////////////////////////////////////////////////////////////////////
    // encode (UserMsg)
    ///////////////////////////////////////////////////////////////////////////
    void UserMsg::encode(System::SerialBuffer& buffer) const
    {
      buffer.append(id);
      buffer.append(name);
    }

    ///////////////////////////////////////////////////////////////////////////
    // decode (GameInfoMsg)
    ///////////////////////////////////////////////////////////////////////////
    bool GameInfoMsg::decode(System::SerialBuffer& buffer)
    {
      bool valid = true;
      valid = valid && buffer.read(id);
      valid = valid && buffer.read(name);
      valid = valid && buffer.read(numPlayers);
      valid = valid && buffer.read(state);
      return valid;
    }

    ///////////////////////////////////////////////////////////////////////////
    // encode (GameInfoMsg)
    ///////////////////////////////////////////////////////////////////////////
    void GameInfoMsg::encode(System::SerialBuffer& buffer) const
    {
      buffer.append(id);
      buffer.append(name);
      buffer.append(numPlayers);
      buffer.append(state);
    }

    ///////////////////////////////////////////////////////////////////////////
    // decode (OpenGamesMsg)
    ///////////////////////////////////////////////////////////////////////////
    bool OpenGamesMsg::decode(System::SerialBuffer& buffer)
    {
      bool valid = true;
      size_t count = 0;
      games.clear();
      valid = valid && buffer.read(count);
      for(size_t i = 0; i < count && valid; ++i)
      {
        auto& game = games.emplace_back();
        valid = valid && game.decode(buffer);
      }
      return valid;
    }

    ///////////////////////////////////////////////////////////////////////////
    // encode (OpenGamesMsg)
    ///////////////////////////////////////////////////////////////////////////
    void OpenGamesMsg::encode(System::SerialBuffer& buffer) const
    {
      buffer.append(games.size());
      for(auto& game : games)
      {
        game.encode(buffer);
      }
    }

    ///////////////////////////////////////////////////////////////////////////
    // decode (RequestJoinGameMsg)
    ///////////////////////////////////////////////////////////////////////////
    bool RequestJoinGameMsg::decode(System::SerialBuffer& buffer)
    {
      bool valid = true;
      valid = valid && buffer.read(gameID);
      return valid;
    }

    ///////////////////////////////////////////////////////////////////////////
    // encode (RequestJoinGameMsg)
    ///////////////////////////////////////////////////////////////////////////
    void RequestJoinGameMsg::encode(System::SerialBuffer& buffer) const
    {
      buffer.append(gameID);
    }

    ///////////////////////////////////////////////////////////////////////////
    // decode (GameMembersMsg)
    ///////////////////////////////////////////////////////////////////////////
    bool GameMembersMsg::decode(System::SerialBuffer& buffer)
    {
      bool valid = true;
      size_t numPlayers = 0;
      players.clear();
      valid = valid && buffer.read(gameID);
      valid = valid && buffer.read(numPlayers);
      for(size_t i = 0; i < numPlayers && valid; ++i)
      {
        valid = valid && players.emplace_back().decode(buffer);
      }
      return valid;
    }

    ///////////////////////////////////////////////////////////////////////////
    // encode (GameMembersMsg)
    ///////////////////////////////////////////////////////////////////////////
    void GameMembersMsg::encode(System::SerialBuffer& buffer) const
    {
      buffer.append(gameID);
      buffer.append(players.size());
      for(auto& player : players)
      {
        player.encode(buffer);
      }
    }

    ///////////////////////////////////////////////////////////////////////////
    // decode (DeckMsg)
    ///////////////////////////////////////////////////////////////////////////
    bool DeckMsg::decode(System::SerialBuffer& buffer)
    {
      bool valid = true;
      size_t numCards = 0;
      cards.clear();
      valid = valid && buffer.read(locked);
      valid = valid && buffer.read(numCards);
      for(size_t i = 0; i < numCards && valid; ++i)
      {
        valid = valid && cards.emplace_back().decode(buffer);
      }
      return valid;
    }

    ///////////////////////////////////////////////////////////////////////////
    // encode (DeckMsg)
    ///////////////////////////////////////////////////////////////////////////
    void DeckMsg::encode(System::SerialBuffer& buffer) const
    {
      buffer.append(locked);
      buffer.append(cards.size());
      for(auto& card : cards)
      {
        card.encode(buffer);
      }
    }

    ///////////////////////////////////////////////////////////////////////////
    // decode (GameStateMsg)
    ///////////////////////////////////////////////////////////////////////////
    bool GameStateMsg::decode(System::SerialBuffer& buffer)
    {
      bool valid = true;
      size_t numPlayers = 0;
      players.clear();
      valid = valid && buffer.read(gameID);
      valid = valid && buffer.read(startPlayerID);
      valid = valid && buffer.read(currentPlayerID);
      valid = valid && buffer.read(winnerID);
      valid = valid && buffer.read(state);
      valid = valid && drawPile.decode(buffer);
      valid = valid && discardPile.decode(buffer);
      valid = valid && buffer.read(numPlayers);
      for(size_t i = 0; i < numPlayers && valid; ++i)
      {
        valid = valid && players.emplace_back().decode(buffer);
      }
      return valid;
    }

    ///////////////////////////////////////////////////////////////////////////
    // encode (GameStateMsg)
    ///////////////////////////////////////////////////////////////////////////
    void GameStateMsg::encode(System::SerialBuffer& buffer) const
    {
      buffer.append(gameID);
      buffer.append(startPlayerID);
      buffer.append(currentPlayerID);
      buffer.append(winnerID);
      buffer.append(state);
      drawPile.encode(buffer);
      discardPile.encode(buffer);
      buffer.append(players.size());
      for(auto& player : players)
      {
        player.encode(buffer);
      }
    }

    ///////////////////////////////////////////////////////////////////////////
    // decode (PhaseMsg)
    ///////////////////////////////////////////////////////////////////////////
    bool PhaseMsg::decode(System::SerialBuffer& buffer)
    {
      bool valid = true;
      size_t count = 0;
      matches.clear();
      valid = valid && buffer.read(count);
      for(size_t i = 0; i < count && valid; ++i)
      {
        valid = valid && matches.emplace_back().decode(buffer);
      }
      return valid;
    }

    ///////////////////////////////////////////////////////////////////////////
    // encode (PhaseMsg)
    ///////////////////////////////////////////////////////////////////////////
    void PhaseMsg::encode(System::SerialBuffer& buffer) const
    {
      buffer.append(matches.size());
      for(auto& match : matches)
      {
        match.encode(buffer);
      }
    }

    ///////////////////////////////////////////////////////////////////////////
    // decode (PlayerStateMsg)
    ///////////////////////////////////////////////////////////////////////////
    bool PlayerStateMsg::decode(System::SerialBuffer& buffer)
    {
      bool valid = true;
      valid = valid && buffer.read(playerID);
      valid = valid && buffer.read(score);
      valid = valid && buffer.read(skipped);
      valid = valid && buffer.read(state);
      valid = valid && buffer.read(phaseNum);
      valid = valid && hand.decode(buffer);
      valid = valid && phase.decode(buffer);
      return valid;
    }

    ///////////////////////////////////////////////////////////////////////////
    // encode (PlayerStateMsg)
    ///////////////////////////////////////////////////////////////////////////
    void PlayerStateMsg::encode(System::SerialBuffer& buffer) const
    {
      buffer.append(playerID);
      buffer.append(score);
      buffer.append(skipped);
      buffer.append(state);
      buffer.append(phaseNum);
      hand.encode(buffer);
      phase.encode(buffer);
    }

  } // namespace Messages
} // namespace Phase10
